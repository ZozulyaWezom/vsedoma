const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.copyDirectory('resources/assets/fonts', 'public/assets/fonts');

mix.copyDirectory('resources/assets/img', 'public/assets/img');

mix.js('resources/assets/js/common.js', 'public/assets/js')
    .extract(['jquery-inview', 'jquery']);

mix.sass('resources/assets/sass/common.scss', 'public/assets/css');


mix.browserSync('v.loc');
mix.disableNotifications();
