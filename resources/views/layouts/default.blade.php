<!doctype html>
<html>
<head>
    @include('includes.head')
</head>
<body>
<div class="wrapper">
    <header>
        @include('includes.header')
    </header>

    <div class="main" class="row">
        @yield('content')
    </div>

    <footer>
        @include('includes.footer')
    </footer>

</div>
<script src="{{ mix('assets/js/manifest.js') }}"></script>
<script src="{{ mix('assets/js/vendor.js') }}"></script>
{{--<script src="{{ mix('js/app.js') }}"></script>--}}
<script src="{{ mix('assets/js/common.js') }}"></script>
</body>
</html>
